//
//  UserService.swift
//  TestRessources
//
//  Created by Thibault Wittemberg on 17-01-10.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import Foundation

public class UserService {
    
    var configuration: Configuration
    
    public required init(withConfiguration configuration: Configuration) {
        self.configuration = configuration
    }
    
    public func login () {
        print("Login URL=\(self.configuration.url)")
    }
    
}
