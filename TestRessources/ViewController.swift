//
//  ViewController.swift
//  TestRessources
//
//  Created by Thibault Wittemberg on 17-01-10.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var userService: UserService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.userService.login()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

