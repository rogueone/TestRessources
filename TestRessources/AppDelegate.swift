//
//  AppDelegate.swift
//  TestRessources
//
//  Created by Thibault Wittemberg on 17-01-10.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let container = Container() { container in
        
        // Get the appropriate configuration
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        let configurationProvider = Bundle.main.infoDictionary!["CONFIGURATION_PROVIDER"] as! String
        
        
        guard let any : AnyClass = NSClassFromString(appName + "." + configurationProvider) else {
            return
        }
        
        guard let ns = any as? BaseConfiguration.Type else{
            return
        }
        
        
        let configuration = ns.init()
        
        // inject the configuration into the services
        container.register(Configuration.self) { _ in configuration}
        container.register(UserService.self) { r in
            let conf = r.resolve(Configuration.self)
            let userService = UserService(withConfiguration: conf!)
            return userService
        }
        container.register(ViewController.self) { r in
            let controller = ViewController()
            controller.userService = r.resolve(UserService.self)
            return controller
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Instantiate a window.
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window
        
        // Instantiate the root view controller with dependencies injected by the container.
        window.rootViewController = container.resolve(ViewController.self)
        
        return true

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

