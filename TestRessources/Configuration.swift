//
//  Configuration.swift
//  TestRessources
//
//  Created by Thibault Wittemberg on 17-01-10.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import Foundation

public protocol Configuration {
    var url: String {get}
}
