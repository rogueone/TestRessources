//
//  DevConfiguration.swift
//  TestRessources
//
//  Created by Thibault Wittemberg on 17-01-10.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import Foundation

public class DevConfiguration: BaseConfiguration {
    
    override public var url: String {
        return "www.dev.com"
    }
}
